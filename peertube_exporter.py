#!/usr/bin/env python

import json
import re
import sys
import time
import ssl
try:
    import urllib2
except:
    # Python 3
    import urllib.request as urllib2

from prometheus_client import start_http_server
from prometheus_client.core import Gauge, REGISTRY


class PeerTubeExporter(object):
    def __init__(self, target):
        self._target = target.rstrip("/")
        self._statuses = ['totalLocalVideos', 'totalLocalVideoViews', 'totalLocalVideoFilesSize', 'totalLocalVideoComments', 'totalVideos',
                          'totalVideoComments', 'totalUsers', 'totalInstanceFollowers', 'totalInstanceFollowing']
        self._metrics = {}
        for s in self._statuses:
            self._metrics[s] = Gauge(s, 'count')
        self._context = ssl._create_unverified_context()

    def collect(self):
        stats = json.loads(urllib2.urlopen(
            "{0}/api/v1/server/stats".format(self._target), context=self._context
        ).read().decode("utf-8"))
        for s in stats:
            if s in self._statuses:
                self._metrics[s].set(stats[s])


if __name__ == "__main__":
    a = PeerTubeExporter("https://127.0.0.1/")
    start_http_server(9118)
    while True:
        a.collect()
        time.sleep(5)
